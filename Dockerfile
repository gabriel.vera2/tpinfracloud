FROM ubuntu:latest

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    apache2 \
    wget \
    unzip

RUN wget https://github.com/twbs/bootstrap/releases/download/v5.3.3/bootstrap-5.3.3-examples.zip -O /tmp/bootstrap.zip && \
    unzip /tmp/bootstrap.zip -d /tmp && \
    mv /tmp/bootstrap-5.3.3-examples/product/* /var/www/html/ && \
    mv /tmp/bootstrap-5.3.3-examples/assets /var/www/html/

RUN rm /tmp/bootstrap.zip && rm -rf /tmp/bootstrap-5.3.3-examples

EXPOSE 80 443

CMD ["apache2ctl", "-D", "FOREGROUND"]

