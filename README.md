**TP Infra Cloud Jueves Vera, Gabriel Hernán**

Este trabajo esta orientado para que cualquier usuario pueda ingresar a este repositorio Git, y pueda desplegar
a través de la imagen de Docker disponible, una APP de Bootstrap de prueba ingresando por la IP de su máquina virtual
sobre su navegador de elección montado sobre Apache2.

**PRERREQUISITOS**

- Imagen Ubuntu
- Salida a Internet
- Direccion IP estática (192.168.0.250)
- Usuario local con permisos SUDO

Una vez contemos con los prerrequisitos, damos inicio a la actividad.

Paso 1, instalación de Docker y asignación de permisos a tu usuario local:
```
$ sudo apt upgrade && sudo apt install docker.io -y
$ sudo gpasswd -a usuario docker
```

Paso 2, clonación de repositorio e ingreso al directorio:
```
$ git clone https://gitlab.com/gabriel.vera2/tpinfracloud.git
$ cd tpinfracloud
```

Paso 3, ejecución de contenedor Docker creando la imagen:
```
$ sudo docker build -t tpinfracloud .
```

Paso 4, lanzar contenedor:
```
$ sudo docker run -d -p 80:80 -p 443:443 --name tpinfracloud_container tpinfracloud
```

Paso 5, el sitio web ya es accesible:
```
http:\\192.168.0.250
```
